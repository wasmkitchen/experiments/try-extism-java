# try-extism-java

Go to https://start.vertx.io/
`sdk install java 19.0.1-amzn`

`sdk install java 17.0.5-amzn`

```bash
sdk use java 19.0.1-amzn
LD_LIBRARY_PATH=/usr/local/lib ./mvnw clean compile exec:java
```


pip3 install poetry
pip3 install git+https://github.com/extism/cli
extism --prefix=/usr/local install latest
pip3 install extism
